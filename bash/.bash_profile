#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
export GOPATH=$HOME/gopath
export GOROOT=$HOME/go
