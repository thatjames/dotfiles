#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
PS1='\e[0;0;1m[\u@\h \W]\$ \e[m'
alias vi='nvim '
alias sudo='sudo -E '
alias update='yay -Syu'
alias install='yay -S '
export LS_COLORS="di=1;33:ex=1;4;33:ln=33"

# load user defined functions
if [ -f $HOME/.bash_functions ]; then
    source $HOME/.bash_functions
fi

PATH=$PATH:$HOME/.scripts:$HOME/.local/bin
export EDITOR=nvim
