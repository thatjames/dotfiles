function MarkdownGo()
    if !system('pgrep markdown-go')
        silent !markdown-go -github b922b60a802a81ba93f1a5f8c64bbe80785e7bad &> /dev/null &
    endif
    !firefox localhost:8008/%
endfunction

augroup autocom
    autocmd!
    autocmd VimLeave * !killall markdown-go
augroup end

let mapleader=","
map <leader>p :silent call MarkdownGo()<CR>
